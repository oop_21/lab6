package com.chaiwat.week6;


public class App 
{
    public static void main( String[] args )
    {
        BookBank chaiwat = new BookBank("Chaiwat", 50.0);

        chaiwat.print();
        BookBank prayud = new BookBank("PraYudd", 100000.0);
        prayud.print();
        prayud.withdarw(40000.0);
        prayud.print();

        chaiwat.deposit(40000.0);
        chaiwat.print();

        BookBank prawit = new BookBank("Prawit", 1000000.0);
        prawit.print();
        prawit.deposit(2);
        prawit.print();
    }
}
