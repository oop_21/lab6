package com.chaiwat.week6;

public class RobotApp {

    public static void main(String[] args) {
        RoBot kapong = new RoBot("kapong",'k',0,0);
        kapong.print();


        RoBot blue = new RoBot("Blue", 'B', 1 , 1);
        blue.print();

        for(int y = RoBot.MIN_Y; y <= RoBot.MAX_Y; y++){
            for(int x = RoBot.MIN_X; x <= RoBot.MAX_X; x++){
                if(kapong.getX() == x && kapong.getY() == y){
                    System.out.print(kapong.getSymbol());
                } else if (blue.getX() == x && blue.getY() == y ){
                    System.out.print(blue.getSymbol());
                } else {
                    System.out.print("-");
                }
            }
                System.out.println();
            }
        }
    }

