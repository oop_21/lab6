package com.chaiwat.week6;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldWithdrawSuccess(){
        BookBank book = new BookBank("Chaiwat", 100);
        book.withdarw(50);
        assertEquals(50,book.getBalance(), 0.00001);
    }
    @Test
    public void shouldWithdrawOver(){
        BookBank book = new BookBank("Chaiwat", 100);
        book.withdarw(150);
        assertArrayEquals(100, book.getBalance(),0.00001);
    }
    private void assertArrayEquals(int i, double balance, double d) {
    }   
    
    @Test
    public void shouldWithdrawWihNegativNumber(){
        BookBank book = new BookBank("Chaiwat", 100);
        book.withdarw(-100);
        assertArrayEquals(100, book.getBalance(), 0.00001);
    }
    
}
