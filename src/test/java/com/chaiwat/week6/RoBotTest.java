package com.chaiwat.week6;

import static org.junit.Assert.assertEquals;


import org.junit.Test;

public class RoBotTest {
    @Test
    public void shouldDownOver(){
        RoBot robot = new RoBot("Robot", 'R',0,RoBot.MAX_Y);
        assertEquals(false, robot.down());
        assertEquals(RoBot.MAX_Y, robot.getY());
    }
    @Test
    public void shouldUpNegative(){
        RoBot robot = new RoBot("Robot", 'R',0,RoBot.MIN_Y);
        assertEquals(false, robot.up());
        assertEquals(RoBot.MIN_Y, robot.getY());
    }
    @Test
    public void shouldDownSuccess(){
        RoBot robot = new RoBot("Robot", 'R',0,0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());
    }
    @Test
    public void shouldUpSuccess(){
        RoBot robot = new RoBot("Robot", 'R',0,1);
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY());
    }
    @Test
    public void shouldleft(){
        RoBot robot = new RoBot("Robot", 'R',RoBot.MIN_X,0);
        assertEquals(false, robot.left());
        assertEquals(RoBot.MIN_X, robot.getX());
    }
    @Test
    public void shouldright(){
        RoBot robot = new RoBot("Robot", 'R',RoBot.MAX_X,0);
        assertEquals(false, robot.right());
        assertEquals(RoBot.MAX_X, robot.getX());
    }
    @Test
    public void shouldleftSuccess(){
        RoBot robot = new RoBot("Robot", 'R',0,0);
        assertEquals(true, robot.right());
        assertEquals(1, robot.getX());
    }
    @Test
    public void shouldrightSuccess(){
        RoBot robot = new RoBot("Robot", 'R',1,0);
        assertEquals(true, robot.right());
        assertEquals(2, robot.getX());
    }
    @Test
    public void shouldCreateRobotSuccess2(){
        RoBot robot = new RoBot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }
    @Test
    public void shouldUpSuccess1(){
        RoBot robot = new RoBot("Robot",'R',10,11);
        boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(6, robot.getY());
    }
    @Test
    public void shouldUpSuccess2(){
        RoBot robot = new RoBot("Robot",'R',10,11);
        boolean result = robot.up(11);
        assertEquals(true, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldUpNFail1(){
        RoBot robot = new RoBot("Robot",'R',10,11);
        boolean result = robot.up(12);
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }
    @Test
    public void shouldDownSuccess1(){
        RoBot robot = new RoBot("Robot",'R',0,0);
        boolean result = robot.down(5);
        assertEquals(true, result);
        assertEquals(5, robot.getY());
    }
    @Test
    public void shouldDownNFail(){
        RoBot robot = new RoBot("Robot",'R',0,0);
        boolean result = robot.down(20);
        assertEquals(false, result);
        assertEquals(19, robot.getY());
    }
    @Test
    public void shouldRightSuccess1(){
        RoBot robot = new RoBot("Robot",'R',0,0);
        boolean result = robot.right(5);
        assertEquals(true, result);
        assertEquals(5, robot.getX());
    }
    @Test
    public void shouldRightNFail(){
        RoBot robot = new RoBot("Robot",'R',0,0);
        boolean result = robot.right(25);
        assertEquals(false, result);
        assertEquals(19, robot.getX());
    }
    @Test
    public void shouldLeftSuccess1(){
        RoBot robot = new RoBot("Robot",'R',10,0);
        boolean result = robot.left(5);
        assertEquals(true, result);
        assertEquals(5, robot.getX());
    }
    @Test
    public void shouldLefNFail(){
        RoBot robot = new RoBot("Robot",'R',10,0);
        boolean result = robot.left(20);
        assertEquals(false, result);
        assertEquals(0, robot.getX());
    }
}
